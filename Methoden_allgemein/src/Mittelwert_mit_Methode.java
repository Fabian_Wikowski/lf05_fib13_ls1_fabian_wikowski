
public class Mittelwert_mit_Methode {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double x2 = 5;
      double y2 = 4;
      double m;
      double m2;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      // Methodenaufruf
      m = mittelwert(x, y);										// "aktuelle" Parameter
      m2 = mittelwert(x2,y2);
    		  
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x2, y2, m2);
   }
   
   // als Methode geschrieben
   
   public static double mittelwert(double a, double b) {		// a und b sind formale Parameter a ersetzt x und b ersetzt y
	   return (a + b) / 2.0; 									// letzte Anweisung da Ergebnis ausgegeben werden soll und Methode beendet werden soll
   
   
   }
}
