import java.util.Scanner;

public class AB_Schleifen1_Aufgabe1 {

	public static void main(String[] args) {
		// test2
		System.out.print("Bitte geben Sie eine ganzzahlige Zahl ein: ");

		Scanner tastatur = new Scanner(System.in);

		int eingabe = tastatur.nextInt();

		for (int i = 0; i <= eingabe; i++) {

			if (i != eingabe) {

				System.out.print(i + ", ");
			}

			else
				System.out.print(i);

		}

		System.out.println();

		for (int i = 0; i <= eingabe; i++) {

			if (i != eingabe) {

				System.out.print(i + ", ");
			}

			else
				System.out.print(i);//test
		}

	}

}
