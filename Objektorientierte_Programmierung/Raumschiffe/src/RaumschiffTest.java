
/**
 * @author Fabian Wikowski FIB-13
 * @version 1.0
 */

import java.util.ArrayList;

public class RaumschiffTest {
	public static void main(String[] args) {

		// +------------------------------------------------------------------------------------------------------------------------------+

		// _________________________________________________________________________________________________________________
		// Erzeugung und Manipulierung des Klingonenobjekts
		Ladung kling1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung kling2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Raumschiff klingonen = new Raumschiff();

		klingonen.setTorpedos(1);
		klingonen.setEnergie(100);
		klingonen.setSchutzschilde(100);
		klingonen.setHuelle(100);
		klingonen.setLebenserhaltung(100);
		klingonen.setName("IKS Hegh'ta");
		klingonen.setReparaturRoboter(2);
		klingonen.addLadung(kling1);
		klingonen.addLadung(kling2);
		// _________________________________________________________________________________________________________________

		// _________________________________________________________________________________________________________________
		// Erzeugung und Manipulierung des Romulanerobjekts
		Ladung rom1 = new Ladung("Borg-Schrott", 5);
		Ladung rom2 = new Ladung("Rote Materie", 2);
		Ladung rom3 = new Ladung("Plasma-Waffe", 50);
		Raumschiff romulaner = new Raumschiff();

		romulaner.setTorpedos(2);
		romulaner.setEnergie(100);
		romulaner.setSchutzschilde(100);
		romulaner.setHuelle(100);
		romulaner.setLebenserhaltung(100);
		romulaner.setName("IRW Khazara");
		romulaner.setReparaturRoboter(2);
		romulaner.addLadung(rom1);
		romulaner.addLadung(rom2);
		romulaner.addLadung(rom3);
		// _________________________________________________________________________________________________________________

		// _________________________________________________________________________________________________________________
		// Erzeugung und Manipulierung des Vulkanierobjekts
		Ladung vulk1 = new Ladung("Forschungssonde", 35);
		Ladung vulk2 = new Ladung("Photonentorpedo", 3);
		Raumschiff vulkanier = new Raumschiff();

		vulkanier.setTorpedos(0);
		vulkanier.setEnergie(80);
		vulkanier.setSchutzschilde(80);
		vulkanier.setHuelle(50);
		vulkanier.setLebenserhaltung(100);
		vulkanier.setName("Ni'Var");
		vulkanier.setReparaturRoboter(5);
		vulkanier.addLadung(vulk1);
		vulkanier.addLadung(vulk2);
		// _________________________________________________________________________________________________________________

		// +------------------------------------------------------------------------------------------------------------------------------+

		// Ablauf nach Aufgabe 4.2.1
		System.out.println();

		// _________________________________________________________________________________________________________________
		// Die Klingonen schiessen mit dem Photonentorpedo einmal auf die Romulaner.
		System.out.println("==========================================================");
		System.out.println("|| >>> Angriff (Schuss) der Klingonen auf Romulaner <<< ||");
		System.out.println("==========================================================");
		System.out.println();

		klingonen.photonentorpedosSchiessen(romulaner);
		// _________________________________________________________________________________________________________________

		System.out.println();
		System.out.println();

		// _________________________________________________________________________________________________________________
		// Die Romulaner schiessen mit der Phaserkanone zurueck.
		System.out.println("=============================================================");
		System.out.println("|| >>> Romulaner erwidern das feuern auf die Klingonen <<< ||");
		System.out.println("=============================================================");
		System.out.println();

		romulaner.phaserkanoneSchiessen(klingonen);
		// _________________________________________________________________________________________________________________

		System.out.println();

		// +------------------------------------------------------------------------------------------------------------------------------+
		// Die Vulkanier senden eine Nachricht an Alle "Gewalt ist nicht logisch".
		vulkanier.nachrichtAnAlle(">> Gewalt ist NICHT logisch<<");
		// +------------------------------------------------------------------------------------------------------------------------------+

		System.out.println();

		// _________________________________________________________________________________________________________________
		// Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr
		// Ladungsverzeichnis aus
		System.out.println("------------------------------------------------------------------------");
		System.out.println("|| >>> Uebersicht ueber den Zustand des Raumschiffs der Klingonen <<< ||");
		System.out.println("------------------------------------------------------------------------");
		System.out.println();

		klingonen.zustandRaumschiff();
		// _________________________________________________________________________________________________________________

		System.out.println();

		// _________________________________________________________________________________________________________________
		System.out.println("-------------------------------------------------------------------");
		System.out.println("|| >>> Uebersicht ueber das Ladungsverzeichnis der Klingonen <<< ||");
		System.out.println("-------------------------------------------------------------------");
		System.out.println();

		klingonen.ladungsverzeichnisAusgeben();
		// _________________________________________________________________________________________________________________

		System.out.println();
		System.out.println();

		// _________________________________________________________________________________________________________________
		// Die Vulkanier rüsten ihr Schiff mit Hilfe der Reparatur Roboter auf
		System.out.println("=============================================");
		System.out.println("|| >>> Vulkanier rüsten ihr Schiff auf <<< ||");
		System.out.println("=============================================");

		vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getReparaturRoboter());

		System.out.println();

		// Die Vulkanier verladen Ihre Ladung "Photonentorpedos" in die Torpedoroehren
		// Ihres Raumschiffes und raeumen das Ladungsverzeichnis auf.
		System.out.print("Sollen Photonentorpedos geladen werden? ");
		vulkanier.photonentorpedosLaden(20);
		vulkanier.ladungsverzeichnisAufraeumen();

		// _________________________________________________________________________________________________________________

		System.out.println();
		System.out.println();

		// _________________________________________________________________________________________________________________
		// Die Klingonen schiessen mit zwei weiteren Photonentorpedo auf die Romulaner.
		System.out.println(
				"=====================================================================================");
		System.out
				.println("|| >>> Abschuss von 2 Photonentorpedos seitens der Klingonen auf die Romulaner <<< ||");
		System.out.println(
				"=====================================================================================");
		System.out.println();

		klingonen.photonentorpedosSchiessen(romulaner);
		klingonen.photonentorpedosSchiessen(romulaner);
		// _________________________________________________________________________________________________________________

		System.out.println();

		// +------------------------------------------------------------------------------------------------------------------------------+

		// _________________________________________________________________________________________________________________
		System.out.println("========================================================");
		System.out.println("|| >>> Uebersicht der Zustaende aller Raumschiffe <<< ||");
		System.out.println("========================================================");
		System.out.println();

		// Alle beteiligten Fraktionen (Klingonen, Romulaner und Vulkanier) pruefen
		// ihren Zustand vom Raumschiff und dessen Ladungsverzeichnis.
		System.out.println("-------------------------------------------");
		System.out.println("|| ZUSTAND des Raumschiffs der Klingonen ||");
		System.out.println("-------------------------------------------");
		System.out.println();

		klingonen.zustandRaumschiff();

		System.out.println();

		System.out.println("------------------------------------------------------");
		System.out.println("|| LADUNGSVERZEICHNIS des Raumschiffs der Klingonen ||");
		System.out.println("------------------------------------------------------");
		System.out.println();

		klingonen.ladungsverzeichnisAusgeben();

		// _________________________________________________________________________________________________________________
		System.out.println();
		System.out.println();
		// _________________________________________________________________________________________________________________

		System.out.println("-------------------------------------------");
		System.out.println("|| ZUSTAND des Raumschiffs der Romulaner ||");
		System.out.println("-------------------------------------------");
		System.out.println();

		romulaner.zustandRaumschiff();

		System.out.println();

		System.out.println("------------------------------------------------------");
		System.out.println("|| LADUNGSVERZEICHNIS des Raumschiffs der Romulaner ||");
		System.out.println("------------------------------------------------------");
		System.out.println();

		romulaner.ladungsverzeichnisAusgeben();

		// _________________________________________________________________________________________________________________
		System.out.println();
		System.out.println();
		// _________________________________________________________________________________________________________________

		System.out.println("-------------------------------------------");
		System.out.println("|| ZUSTAND des Raumschiffs der Vulkanier ||");
		System.out.println("-------------------------------------------");
		System.out.println();

		vulkanier.zustandRaumschiff();

		System.out.println();

		System.out.println("------------------------------------------------------");
		System.out.println("|| LADUNGSVERZEICHNIS des Raumschiffs der Vulkanier ||");
		System.out.println("------------------------------------------------------");
		System.out.println();

		vulkanier.ladungsverzeichnisAusgeben();
		// _________________________________________________________________________________________________________________
		System.out.println();
		System.out.println();
		// _________________________________________________________________________________________________________________

		// +------------------------------------------------------------------------------------------------------------------------------+
		// Eine Nachricht an alle soll mit Hilfe des broadcastKommunikator(s) ausgegeben
		// werden.
		System.out.println("=======================================================");
		System.out.println("|| >>> NACHRICHT AN ALLE (broadcastKommunikator) <<< ||");
		System.out.println("=======================================================");
		System.out.println();

		ArrayList<String> broadcastKommunikator = new ArrayList<String>();
		broadcastKommunikator = klingonen.eintraegeLogbuchZurueckgeben();
		System.out.println(broadcastKommunikator);
		// +------------------------------------------------------------------------------------------------------------------------------+
	}
}
