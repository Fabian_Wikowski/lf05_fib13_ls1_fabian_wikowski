
/**
 * @author Fabian Wikowski FIB-13
 * @version 1.0
 */

import java.util.ArrayList;

// Erzeugung der Fachklasse Raumschiff
public class Raumschiff {

    // +=================================================================================================+
    // | ATTRIBUTE
    // +=================================================================================================+
    private String name; // Name des Raumschiffobjekts
    private int energie; // Wert fuer Energiewert des Raumschiffobjekts
    private int schutzschilde; // Wert fuer Schutzschild des Raumschiffobjekts
    private int lebenserhaltung; // Wert fuer Lebenserhaltung des Raumschiffobjekts
    private int huelle; // Wert fuer Huelle Raumschiffobjekts
    private int torpedos; // Wert fuer Anzahl an Torpedos des Raumschiffobjekts
    private int reparaturRoboter; // Wert fuer Anzahl an Reperaturroboter des Raumschiffobjekts
    private static ArrayList<Ladung> ladungsVerzeichnis = new ArrayList<Ladung>(); // Eine Array Liste fuer das
                                                                                   // Ladungsverzeichnis
    private static ArrayList<String> broadcastKommunikator = new ArrayList<String>(); // Eine Array Liste fuer den
                                                                                      // Broadcast an alle
    // --------------------------------------------------------------------------------------------------------------------------------

    // +=================================================================================================+
    // | KONSTRUKTOR(EN)
    // +=================================================================================================+

    // ohne Parameter
    public Raumschiff() {
    }

    // voll parametrisiert
    public Raumschiff(String name, int energie, int schutzschilde, int lebenserhaltung, int huelle, int torpedos,
            int reparaturRoboter) {
        this.name = name;
        this.energie = energie;
        this.schutzschilde = schutzschilde;
        this.lebenserhaltung = lebenserhaltung;
        this.huelle = huelle;
        this.torpedos = torpedos;
        this.reparaturRoboter = reparaturRoboter;
    }
    // --------------------------------------------------------------------------------------------------------------------------------

    // +=================================================================================================+
    // | VERWALTUNGSMETHODEN
    // +=================================================================================================+

    // setter

    // fuer Name
    public void setName(String name) {
        this.name = name;
    }

    // fuer Energie
    public void setEnergie(int energie) {
        this.energie = energie;
    }

    // fuer Schutzschilde
    public void setSchutzschilde(int schilde) {
        this.schutzschilde = schilde;
    }

    // fuer Lebenserhaltung
    public void setLebenserhaltung(int lebenserhaltung) {
        this.lebenserhaltung = lebenserhaltung;
    }

    // fuer Huelle
    public void setHuelle(int huelle) {
        this.huelle = huelle;
    }

    // fuer Torpedos
    public void setTorpedos(int torpedos) {
        this.torpedos = torpedos;
    }

    // fuer Reparatur Roboter
    public void setReparaturRoboter(int reparaturRoboter) {
        this.reparaturRoboter = reparaturRoboter;
    }
    // --------------------------------------------------------------------------------------------------------------------------------

    // --------------------------------------------------------------------------------------------------------------------------------
    // getter

    // fuer Name
    public String getName() {
        return this.name;
    }

    // fuer Energie
    public int getEnergie() {
        return this.energie;
    }

    // fuer Schutzschilde
    public int getSchutzschilde() {
        return this.schutzschilde;
    }

    // fuer Lebenserhaltung
    public int getLebenserhaltung() {
        return this.lebenserhaltung;
    }

    // fuer Huelle
    public int getHuelle() {
        return this.huelle;
    }

    // fuer Torpedos
    public int getTorpedos() {
        return this.torpedos;
    }

    // fuer Reparatur Roboter
    public int getReparaturRoboter() {
        return this.reparaturRoboter;
    }
    // --------------------------------------------------------------------------------------------------------------------------------

    // +=================================================================================================+
    // | METHODEN
    // +=================================================================================================+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Ladung soll dem Objekt hinzugefuegt werden
     * 
     * @param ladung
     */
    public void addLadung(Ladung ladung) {
        this.ladungsVerzeichnis.add(ladung);
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Torpedos sollen vom Raumschiff abgeschossen werden
     * Wenn keine vorhanden sind dann soll -=*Click*=- ausgegeben werden
     * 
     * @param raumschiff
     */
    public void photonentorpedosSchiessen(Raumschiff raumschiff) {
        if (this.torpedos > 0) {
            this.nachrichtAnAlle("Photonentorpedo abgeschossen");
            raumschiff.treffer(raumschiff);
            this.torpedos--;
        } else {
            System.out.println("-=*Click*=-");
        }
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Die Ladung der Torpedos innerhalb des Raumschiffs, welche als String
     * gespeichert sind sollen zum Datentyp Integer konvertiert werden wenn sie
     * beladen ist
     * 
     * Ansonsten wenn es nicht beladen ist soll Keine Photonentorpedos gefunden! und
     * -=*Click*=- ausgegeben werden
     * 
     * @param anzahlTorpedos
     */

    public void photonentorpedosLaden(int anzahlTorpedos) {

        // Basis Variablen definieren um sie später zu nutzen
        boolean besitztTorpedos = false;
        int index = 0;
        int verladen = 0;

        // Es wird das Ladungsverzeichnis durchsucht und geprüft ob Torpedos als Ladung
        // existieren und ob die Anzahl mehr als 0 ist
        for (int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
            if (this.ladungsVerzeichnis.get(i).getBezeichnung() == "Photonentorpedo"
                    && this.ladungsVerzeichnis.get(i).getMenge() > 0) {
                besitztTorpedos = true;
                index = i;
            }
        }

        // Wenn keine Torpedos vorhanden sind soll eine Nachricht ausgegeben werden
        if (!besitztTorpedos) {
            System.out.println("Keine Photonentorpedos gefunden!");
            this.nachrichtAnAlle("-=*Click*=-");
        } else {
            // Wenn die aufzuladende Anzahl an Torpedos groesser ist als, die die vorhanden
            // ist, werden alle geladen
            System.out.println("ja");
            if (anzahlTorpedos > this.ladungsVerzeichnis.get(index).getMenge()) {
                verladen = this.ladungsVerzeichnis.get(index).getMenge();
                this.torpedos += verladen;
                this.ladungsVerzeichnis.get(index).setMenge(0);
            } else {
                verladen = this.ladungsVerzeichnis.get(index).getMenge();
                this.torpedos += verladen;
                this.ladungsVerzeichnis.get(index)
                        .setMenge(this.ladungsVerzeichnis.get(index).getMenge() - anzahlTorpedos);
            }

            System.out.printf("Es wurden [%d] Photonentorpedo(s) eingesetzt!\n", verladen);
        }
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Phaserkanonen sollen vom Raumschiff abgeschossen werden und entsprechenden
     * Schaden anrichten
     * 
     * Ansonsten -=*Click*=- ausgeben
     * 
     * @param raumschiff
     */
    public void phaserkanoneSchiessen(Raumschiff raumschiff) {
        if (this.energie >= 50) {
            this.energie -= (int) (this.energie * 0.50);
            raumschiff.treffer(raumschiff);
        } else {
            System.out.println("-=*Click*=-");
        }
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Methode die Dinge ausführt, welche passieren sollen wenn das Raumschiff
     * getroffen wird
     * 
     * @param raumschiff
     */

    private void treffer(Raumschiff raumschiff) {
        // Wenn das Schiff getroffen wird soll Name und entsprechender Text ausgegeben
        // werden
        System.out.println(raumschiff.getName() + " wurde getroffen!");

        // Wenn Raumschiff getroffen wird sollen die Schutzschilde um 50% reduziert
        // werden
        raumschiff.setSchutzschilde((int) (raumschiff.getSchutzschilde() * 0.50));

        // Abfrage ob Schilde des Raumschiffs vollstaendig zerstoert sind
        // Huelle und Energieversorgung sollen um 50% reduziert werden
        if (raumschiff.getSchutzschilde() <= 0) {
            raumschiff.setEnergie((int) (raumschiff.getEnergie() * 0.50));
            raumschiff.setHuelle((int) (raumschiff.getHuelle() * 0.50));

            // Wenn die Huelle auf 0 ist soll die Nachricht ausgegeben werden
            // Lebenserhaltungssysteme zerstoert
            if (raumschiff.getHuelle() <= 0) {
                raumschiff.setLebenserhaltung(0);
                // Nachricht an Alle ausgegeben
                raumschiff.nachrichtAnAlle("Lebenserhaltungssysteme wurde voellig zerstoert");
            }
        }
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Hinzufuegen einer Nachricht zum Array um eine Nachricht an alle ausgeben zu
     * koennen
     * 
     * @param message
     */

    public void nachrichtAnAlle(String message) {
        this.broadcastKommunikator.add(message);
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Es soll die erstellte ArrayList zurueckgegeben werden
     * 
     * @return broadcastKommunikator
     */

    public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return broadcastKommunikator;
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Es soll die Reparatur der Systeme durchgefuehrt werden
     * 
     * @param schutzschilde
     * @param energieversorgung
     * @param schiffshuelle
     * @param anzahlDroiden
     */

    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
            int anzahlDroiden) {
        // eine zufällige Zahl mit Random generieren
        double myRandom = Math.random();
        int anzahl = 0;

        // Wenn mehr Droiden im Parameter angegeben als wir haben, sollen alle geladen
        // werden
        if (anzahlDroiden >= this.reparaturRoboter) {
            anzahlDroiden = this.reparaturRoboter;
        }

        // Pruefung welche der einzelnen Parameter zutreffend sind und hinzufuegen der
        // Anzahl hinzu fuer die
        // Berechnung der Reparatur
        if (schutzschilde) {
            anzahl += 1;
        }

        if (energieversorgung) {
            anzahl += 1;
        }

        if (schiffshuelle) {
            anzahl += 1;
        }

        // Berechnung von dem Ergebnis, (Random Zahl * anzahl der Droiden) / anzahl der
        // Systeme
        double ergebnis = (myRandom * anzahlDroiden) / anzahl;

        // Pruefung welche der Parameter zutreffend sind, addiere das Ergebnis zu allen
        // zutreffenden Parametern
        // hinzu
        if (schutzschilde) {
            this.schutzschilde += ergebnis;
        }

        if (energieversorgung) {
            this.energie += ergebnis;
        }

        if (schiffshuelle) {
            this.huelle += ergebnis;
        }
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Es soll der insgesamte Zustand des Raumschiffes ausgegeben werden ausser
     * Ladungsverzeichnis und Broadcastkommunikator diese in separaten Methoden
     */

    public void zustandRaumschiff() {
        System.out.printf(
                "Name des Raumschiffs: %s \n"
                        + "Vorhandene Energie: %s \n"
                        + "Bestehendes Schild: %s \n"
                        + "Lebenserhaltungssysteme: %s \n"
                        + "Bestehende Huelle: %s \n"
                        + "Anzahl an Torpedos: %s \n"
                        + "Reparatur Roboter an Bord: %s \n",
                this.name,
                this.energie,
                this.schutzschilde,
                this.lebenserhaltung,
                this.huelle,
                this.torpedos,
                this.reparaturRoboter);
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+

    /**
     * Es soll das Ladungsverzeichnis ausgegeben werden
     */

    public void ladungsverzeichnisAusgeben() {
        System.out.println(this.ladungsVerzeichnis.toString());
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

    // +------------------------------------------------------------------------------------------------------------------------------+
    // Ladungsverzeichnis wird mit for durchsucht und wenn etwas mit menge=0
    // gefunden wird und geloescht wird, index-1 um die neue Laenge zu beachten

    /**
     * Es sollen Inhalte der Ladung, welche den Wert 0 enthalten aus dem Verzeichnis
     * entfernt werden
     */
    public void ladungsverzeichnisAufraeumen() {
        for (int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
            if (this.ladungsVerzeichnis.get(i).getMenge() <= 0) {
                this.ladungsVerzeichnis.remove(i);
                i--;
            }
        }
    }
    // +------------------------------------------------------------------------------------------------------------------------------+

}
// --------------------------------------------------------------------------------------------------------------------------------
