
/**
 * @author Fabian Wikowski FIB-13
 * @version 1.0
 */

// Erzeugung der Fachklasse Ladung
public class Ladung {

	// +=================================================================================================+
	// | ATTRIBUTE
	// +=================================================================================================+

	private String bezeichnung;
	private int menge;

	// +=================================================================================================+
	// | KONSTRUKTOR(EN)
	// +=================================================================================================+

	public Ladung() {
	}

	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	// +=================================================================================================+
	// | VERWALTUNGSMETHODEN
	// +=================================================================================================+

	// _________________________________________________________________________________________________________________
	// setter
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}
	// _________________________________________________________________________________________________________________

	// _________________________________________________________________________________________________________________
	// getter
	public String getBezeichnung() {
		return this.bezeichnung;
	}

	public int getMenge() {
		return this.menge;
	}
	// _________________________________________________________________________________________________________________

	// +=================================================================================================+
	// | METHODEN
	// +=================================================================================================+

	@Override
	public String toString() {
		return this.bezeichnung + ": " + this.menge;
	}
}

// return "Name: " + Ladung.class.getName() + " \nBezeichnung: " +
// this.bezeichnung + " \nMenge: " + this.menge + " \n";
