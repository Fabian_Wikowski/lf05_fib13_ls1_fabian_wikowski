
public class konto {

	// Attribute
	private String iban;
	private int kontonr;
	private double kontostand;

	// Konstruktoren

	// Konto
	public konto() {
	}

	// IBAN
	public konto(String iban) {
		this.iban = iban;
	}

	// Kontonummer
	public konto(int kontonr) {
		this.kontonr = kontonr;
	}

	// Kontostand
	public konto(double kontostand) {
		this.kontostand = kontostand;
	}

	// voll parametrisierter Konstruktor für das Konto
	public konto(String iban, int kontonr, double kontostand) {
		this.iban = iban;
		this.kontonr = kontonr;
		this.kontostand = kontostand;
	}

	// setter

	// IBAN
	public void setIban(String iban) {
		this.iban = iban;
	}

	// Kontonummer
	public void setKontonr(int kontonr) {
		this.kontonr = kontonr;
	}

	// OPTIONAL setzen von Kontostand
	/*
	 * public void setKontostand(double kontostand) {
	 * this.kontostand = kontostand;
	 * }
	 */
	// getter

	// IBAN
	public String getIban() {
		return this.iban;
	}

	// Kontonummer
	public int getKontonr() {
		return this.kontonr;
	}

	// Kontostand
	public double getKontostand() {
		return this.kontostand;
	}

	// Methoden

	// Geld abheben
	public void geldAbheben(double menge) {
		this.kontostand -= menge;
	}

	// Geld einzahlen
	public void geldEinzahlen(double menge) {
		this.kontostand += menge;
	}

	// Geld ueberweisen
	public void geldUeberweisen(double menge, konto ziel) {
		this.kontostand -= menge;
		ziel.geldEinzahlen(menge);
	}
}
