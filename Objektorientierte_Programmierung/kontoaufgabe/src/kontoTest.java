
public class kontoTest {

	public static void main(String[] args) {
		// Schritt 1 Objekte instanzen erstellen
		konto erstes_konto = new konto();
		konto zweites_konto = new konto("DE500", 0002, 100.19);
		besitzer b1 = new besitzer();

		// Objektmanipulierung
		erstes_konto.setIban("DE499");
		erstes_konto.setKontonr(0001);

		// obtionales Setzen des Kontostandes
		// erstes_konto.setKontostand(10.99);

		b1.setKonto1(erstes_konto);
		b1.setKonto2(zweites_konto);

		// Methoden Test
		erstes_konto.geldUeberweisen(30.00, zweites_konto);

		// Konto Ausgaben
		System.out.println("-- Konto Ausgaben --");
		System.out.println("IBAN: " + erstes_konto.getIban());
		System.out.println("K NR: " + erstes_konto.getKontonr());
		System.out.println("STAND: " + erstes_konto.getKontostand());

		System.out.println("---");

		System.out.println("IBAN: " + zweites_konto.getIban());
		System.out.println("K NR: " + zweites_konto.getKontonr());
		System.out.println("STAND: " + zweites_konto.getKontostand());

		// Besitzer Ausgaben
		System.out.println("-- besitzer Ausgaben --");

		b1.gesamtUebersicht();
		System.out.println("Gesamt: " + b1.gesamtGeld() + " EURO");

	}

}
