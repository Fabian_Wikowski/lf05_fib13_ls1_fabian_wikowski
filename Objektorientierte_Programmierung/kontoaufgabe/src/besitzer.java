public class besitzer {
	private String vorname;
	private String nachname;
	private konto erstes_konto;
	private konto zweites_konto;

	// Konstrukor

	// Besitzer ohne Parameter
	public besitzer() {
	}

	// Besitzer mit 1 Konto
	public besitzer(konto erstes_konto) {
		this.erstes_konto = erstes_konto;
	}

	// Besitzer mit 2 Konten
	public besitzer(konto erstes_konto, konto zweites_konto) {
		this.erstes_konto = erstes_konto;
		this.zweites_konto = zweites_konto;
	}

	// Besitzer mit Nachname
	public besitzer(String nachname) {
		this.nachname = nachname;
	}

	// Voll parametrisierter Besitzer
	public besitzer(String vorname, String nachname, konto erstes_konto, konto zweites_konto) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.erstes_konto = erstes_konto;
		this.zweites_konto = zweites_konto;
	}

	// setter

	// Vorname
	public void setVorname(String name) {
		this.vorname = name;
	}

	// Nachname
	public void setNachname(String name) {
		this.nachname = name;
	}

	// Erstes Konto
	public void setKonto1(konto eingabe) {
		this.erstes_konto = eingabe;
	}

	// Zweites Konto
	public void setKonto2(konto eingabe) {
		this.zweites_konto = eingabe;
	}

	// getter

	// Vorname
	public String getVorname() {
		return this.vorname;
	}

	// Nachname
	public String getNachname() {
		return this.nachname;
	}

	// Erstes Konto
	public konto getKonto1() {
		return this.erstes_konto;
	}

	// Zweites Konto
	public konto getKonto2() {
		return this.zweites_konto;
	}

	// Methoden

	// Ausgabe einer Gesamtuebersicht
	public void gesamtUebersicht() {

		// Ausgabe Erstes Konto
		System.out.println("Konto 1");
		System.out.println("IBAN: " + erstes_konto.getIban());
		System.out.println("Nr.: " + erstes_konto.getKontonr());
		System.out.println("Balance: " + erstes_konto.getKontostand() + " EURO \n");

		// Ausgabe Zweites Konto
		System.out.println("Konto 2");
		System.out.println("IBAN: " + zweites_konto.getIban());
		System.out.println("Nr.: " + zweites_konto.getKontonr());
		System.out.println("Balance: " + zweites_konto.getKontostand() + " EURO \n");
	}

	// Berechnung des verfuegbaren Geldes
	public double gesamtGeld() {
		return (erstes_konto.getKontostand() + zweites_konto.getKontostand());
	}

}
