import java.util.Scanner;

public class Aufgabe_3_2_EinsteigerAB_UrlaubMethode {

	public static void main(String[] args) { // main Methode

		Scanner tastatur = new Scanner(System.in);

		double reisebudget = budget();

		System.out.println();

		berechnungen(reisebudget); // hghg

	}

	public static double geldinusa(double a) { // methode zur Berechnugn des erhaltenen Dolalr Werts
		return a * 1.22;
	}

	public static double geldinjapan(double a) {
		return a * 126.50;
	}

	public static double budget() {
		Scanner tastatur = new Scanner(System.in); // erstellung scanner in main

		System.out.print("Bitte geben Sie Ihr Reisebudget in EURO an: ");
		double reisebudget = tastatur.nextDouble();

		return reisebudget;
	}

	public static void berechnungen(double reisebudget) {
		Scanner tastatur = new Scanner(System.in);

		double umtausch; // variable umtausch in main
		String zielLand; // variable zielland in main
		boolean genugGeld;

		while (reisebudget > 10) { // solange reisebudget > 0 tue das... in main

			System.out.print("In welches Land m�chten Sie reisen? (USA, Japan, England, Schweiz oder Schweden) ");
			zielLand = tastatur.next(); // eingabe zielland in main

			System.out.println();

			zielLand = zielLand.toLowerCase(); // umwandlung eingabe in lower case in main

			switch (zielLand) { // case auswahl zielland in main

			case "usa": // case usa in main
				System.out.println(
						"Sie m�chten also in die USA reisen.\nSie erhalten momentan f�r 1 EURO insgesamt 1,22 DOLLAR. \n");

				char bestaetigung = 'n'; // vorerste deklarierung von variable bestaetigung in main

				while (bestaetigung != 'j') { // pr�fung ob bestaetigung ungleich j in main
					System.out.print("Wie viel Geld m�chten Sie umtauschen? ");
					umtausch = tastatur.nextDouble(); // initialisierung variable umtausch in main durch eingabe

					System.out.println();

					if (genugGeld = umtausch <= reisebudget) { // pr�fung ob genug geld vorhanden in main

						double umtauschUSA; // deklarierung der Variable umtauschUSA in main

						umtauschUSA = geldinusa(umtausch); // methodenaufruf geldinusa -> ausgerechneter wert wird in
															// umtauschUSA gespeuchert in main

						System.out.printf("Sie w�rden f�r Ihre %.2f EURO insgesamt ", umtausch);
						System.out.printf("%.2f DOLLAR erhalten. \n", umtauschUSA);

						System.out.println();

						System.out.printf("M�chten Sie Ihre: %.2f EURO umtauschen? ", umtausch);

						bestaetigung = tastatur.next().charAt(0); // deklarierung variable bestaetigung mit Betrachtung
																	// des ersten Buchstabens in main

						System.out.println();

						if (bestaetigung == 'j') { // Pr�fung ob erster Buchstabe j ist in main
							System.out.printf("Sie erhalten: %.2f DOLLAR \n", umtauschUSA);
							reisebudget = reisebudget - umtausch; // initialisierung variable reisebudget mit erhaltener
																	// differenz in main

							System.out.printf("Ihr restliches Reisebudget lautet %.2f EURO", reisebudget);
							System.out.println();
							System.out.println();

						}
					}
				}
				break;

			case "japan":
				System.out.println(
						"Sie m�chten also nach Japan reisen.\nSie erhalten momentan f�r 1 EURO insgesamt 126,50 YEN. \n");

				char bestaetigung2 = 'n';

				while (bestaetigung2 != 'j')
					System.out.print("Wie viel Geld m�chten Sie umtauschen? ");

				umtausch = tastatur.nextDouble();

				System.out.println();

				if (genugGeld = umtausch <= reisebudget) {

					double umtauschJAPAN;

					umtauschJAPAN = geldinjapan(umtausch);

					System.out.printf("Sie w�rden f�r Ihre %.2f EURO insgesamt ", umtausch);
					System.out.printf("%.2f DOLLAR erhalten. \n", umtauschJAPAN);

					System.out.println();

					System.out.printf("M�chten Sie Ihre: %.2f EURO umtauschen? ", umtausch);

					bestaetigung2 = tastatur.next().charAt(0);

					System.out.println();

					if (bestaetigung2 == 'j') {
						System.out.printf("Sie erhalten: %.2f DOLLAR \n", umtauschJAPAN);
						reisebudget = reisebudget - umtausch;

						System.out.printf("Ihr restliches Reisebudget lautet %.2f EURO", reisebudget);
						System.out.println();
						System.out.println();

						break;
							
					}
				}
			}
			break;
			
			
			
			
			// case "england":
			// sadsadsa

			// case "schweiz":
			// sadadas

			// case "schweden":
			// sadadas

		}

	}

}
