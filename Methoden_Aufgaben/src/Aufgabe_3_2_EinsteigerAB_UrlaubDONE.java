import java.util.Scanner;
import java.util.HashMap; //bonus
import java.util.Map; // bonus

public class Aufgabe_3_2_EinsteigerAB_UrlaubDONE {

	public static void main(String[] args) {
		
		HashMap<String, Waehrung> waehrungen = erstellewaehrungsliste();
		
		Scanner tastatur	= new Scanner(System.in);
		double reisebudget 	= budget(tastatur);
		double limit		= 100.00;
		
		
	while(reisebudget > limit) {
		
		String reise 		= reiseziel(tastatur);			// wohin willst du reisen
		Waehrung waehrung	= waehrungen.get(reise);
		
		System.out.println();
		
		System.out.print("Sie m�chten also nach " + reise + "\n");
		
		System.out.println();
		
		String formatgeld = String.format("%.2f", waehrung.wert) + " " + waehrung.einheit;
		
		System.out.print("Hier gilt momentan der Wechselkurs: 1.00 EURO ist " + formatgeld + " wert");
		
		System.out.println();
		
		System.out.print("Wie viel Geld m�chten Sie umtauschen? ");
		
		reisebudget = umtauschen(tastatur, waehrung, reisebudget, limit);
		
		System.out.println();
		System.out.println();
		
		
	}
		System.out.print("GO HOME");
		
		
		tastatur.close();
		
		
	}

// Methoden

	// Eingabe von budget
	public static double budget(Scanner tastatur) {

		System.out.print("Bitte geben Sie Ihr Reisebudget in EURO an: ");
		double reisebudget = tastatur.nextDouble();

		return reisebudget;
	}

	// W�hrungen
	public static HashMap<String, Waehrung> erstellewaehrungsliste() {

		HashMap<String, Waehrung> map = new HashMap<String, Waehrung>();

		Waehrung usa = new Waehrung("usa", 1.22,"DOLLAR");
		map.put("usa", usa);
		
		Waehrung japan = new Waehrung("japan", 126.50,"YEN");
		map.put("japan", japan);
		
		Waehrung england = new Waehrung("england", 0.89,"PFUND");
		map.put("england", england);
		
		Waehrung schweiz = new Waehrung("schweiz", 1.08,"FRANKEN");
		map.put("schweiz", schweiz);
		
		Waehrung schweden = new Waehrung("schweden", 10.10,"KRONEN");
		map.put("schweden", schweden);

		return map;
	}

	// Reiseziel
	public static String reiseziel(Scanner tastatur) {

		String zielland;

		System.out.print("In welches Land m�chten Sie reisen? (USA, Japan, England, Schweiz oder Schweden) ");

		zielland = tastatur.next();

		zielland = zielland.toLowerCase();

		return zielland;
	}

	public static double umtauschen(Scanner tastatur, Waehrung waehrung, double reisebudget, double limit) {
		
		double umtausch = tastatur.nextDouble();
		
		System.out.println();
		
		double umtauschLand = umtausch * waehrung.wert;
		
		String umtauschbetrag = String.format("%.2f", umtauschLand) + " " + waehrung.einheit;
		
		
		if(umtausch < reisebudget & (reisebudget - umtausch) >= limit ) {
			System.out.print("Sie w�rden insgesamt " + umtauschbetrag + " erhalten");
			
			reisebudget = reisebudget - umtausch;
			
			System.out.println();
			
			System.out.printf("Ihr �briges Reisebudget betr�gt: %.2f EURO", reisebudget);
		}
		
		else {
			System.out.println("Ihr Budget reicht nicht aus!");	
		}
			
		
		return reisebudget;
			 
	}

}
