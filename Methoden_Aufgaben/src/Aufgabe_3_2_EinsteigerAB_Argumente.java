public class Aufgabe_3_2_EinsteigerAB_Argumente {
	public static void main(String[] args) {
		
		ausgabe(1, "Mana");
		ausgabe(2, "Elise");
		ausgabe(3, "Johanna");
		ausgabe(4, "Felizitas");
		ausgabe(5, "Karla");
		
// springt in Methode vergleichen rein
		System.out.println(vergleichen(1, 2));
		System.out.println(vergleichen(1, 5));
		System.out.println(vergleichen(3, 4));
	}
// Methode ausgabe wird festgelegt
public static void ausgabe(int zahl, String name) {				// zahl = a		und		name = b
	System.out.println(zahl + ": " + name);
	
}
// MEthode vergleichen wird festgelegt
public static boolean vergleichen(int arg1, int arg2) {			// arg1 = c		und		arg2 = d
	return (arg1 + 8) < (arg2 * 3);								// arg1 = 1		und		arg2 = 2
}

}