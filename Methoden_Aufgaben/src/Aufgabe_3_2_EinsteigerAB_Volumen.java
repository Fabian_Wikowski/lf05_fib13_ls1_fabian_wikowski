
public class Aufgabe_3_2_EinsteigerAB_Volumen {

	public static void main(String[] args) {
		
		double breite	= 2.5;
		double hoehe	= 5.25;
		double laenge	= 8.5;
		double radius	= 4;
		
		double volumenWuerfel;
		double volumenQuader;
		double volumenPyramide;
		double volumenKugel;
		
		volumenWuerfel	= wuerfel(breite);
		volumenQuader 	= quader(breite, hoehe, laenge);
		volumenPyramide = pyramide(breite, hoehe);
		volumenKugel	= kugel(radius);
		
		System.out.println("Das Volumen eines W�rfels mit den Ma�en: " + breite + "cm in der Breite, " + hoehe + "cm in der H�he und " + laenge + "cm in der L�nge betr�gt: " + volumenWuerfel);
		System.out.println();
		System.out.println("Das Volumen eines Quaders mit den Ma�en: " + breite + "cm in der Breite, " + hoehe + "cm in der H�he und " + laenge + "cm in der L�nge betr�gt: " + volumenQuader);
		System.out.println();
		System.out.println("Das Volumen einer Pyramide mit den Ma�en: " + breite + "cm in der Breite, " + hoehe + "cm in der H�he und " + laenge + "cm in der L�nge betr�gt: " + volumenPyramide);
		System.out.println();
		System.out.println("Das Volumen einer Kugel mit einem Radius von: " + radius + " cm betr�gt: " + volumenKugel);
	}

	public static double wuerfel(double a) {
		return a * a * a;
	}
	
	public static double quader(double a, double b, double c) {
		return a * b * c;
	}
	
	public static double pyramide(double a, double b) {
		return (a * a * b) / 3;
	}
	
	public static double kugel(double a) {
		return (4/3) * (a*a*a) * Math.PI ;
	}
}