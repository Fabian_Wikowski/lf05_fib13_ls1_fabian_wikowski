public class arrayHelper {

	public static void main(String[] args) {
		// Ganzzahlenarray
		int[] zahlen = { 2, 3, 4, 5, 6, 7 };

		// Aufgabe 1
		String ergebnisAufgabe1 = convertArrayToString(zahlen);
		System.out.println(ergebnisAufgabe1);

		// Aufgabe 2
		int[] ergebnisAufgabe2 = umgekehrtesArray(zahlen);
		System.out.println(java.util.Arrays.toString(ergebnisAufgabe2));

		// Aufgabe 3
		int[] ergebnisAufgabe3 = umgekehrtesArrayNeu(zahlen);
		System.out.println(java.util.Arrays.toString(ergebnisAufgabe3));

		// Aufgabe 4
		double[][] ergebnisAufgabe4 = temperaturTabelle(6);
	}

	// Aufgabe 1
	public static String convertArrayToString(int[] zahlen) {
		String liste = "";

		for (int i = 0; i < zahlen.length; i++) {
			liste += zahlen[i];

			if (i == zahlen.length - 1) {
			} else {
				liste += ", ";
			}
		}
		return liste;
	}

	// Aufgabe 2
	public static int[] umgekehrtesArray(int[] zahlen) {
		int x, y;

		for (int i = 0; i < zahlen.length / 2; i++) {
			// Pos 1
			x = zahlen[i];
			// Pos 2
			y = zahlen[(zahlen.length - i) - 1];

			zahlen[i] = y;
			zahlen[zahlen.length - i - 1] = x;
		}
		return zahlen;
	}

	// Aufgabe 3
	public static int[] umgekehrtesArrayNeu(int[] zahlen) {
		int[] umgekehrt = new int[zahlen.length];
		byte x = 0;

		for (int i = zahlen.length - 1; i >= 0; i--) {
			umgekehrt[x] = zahlen[i];
			x += 1;
		}
		return umgekehrt;
	}

	// Aufgabe 4
	public static double[][] temperaturTabelle(int berechnung) {
		// 1. [] Fahrenheit / Celsius
		// 2. [] Werte
		// => [0][0] = Fahrenheit/0.0
		// => [1][0] = Celsius/-17.77
		double[][] tabelle = new double[2][berechnung];
		tabelle[0][0] = 0.00;

		for (int i = 0; i < berechnung; i++) {
			if (i > 0) {
				tabelle[0][i] = tabelle[0][i - 1] + 10.0;
			}
			tabelle[1][i] = (5.0 / 9.0) * (tabelle[0][i] - 32);
		}

		tabelleErstellen(tabelle, berechnung);
		return tabelle;
	}

	public static void tabelleErstellen(double[][] tabelle, int berechnung) {
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("|");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("-----------------------\n");

		for (int i = 0; i < berechnung; i++) {
			// Fahrenheit
			System.out.printf("%-12s", tabelle[0][i]);
			System.out.printf("|");

			// Celsius
			System.out.printf("%4s", "");
			System.out.printf("%.2f\n", tabelle[1][i]);
		}

	}

	// Aufgabe 5
	public static void nmMatrix() {

	}

}
