
public class AllgemeineBeispieleEinfuehrung {

	public static void main(String[] args) {
		// test2
		int x = 3;
		int y = 5;
		int z = 7;
		
		// if Verzweigung
		
		if(x < y) {
			
			System.out.println(z);
			
		}
		
		if(x > z) {
			
			System.out.println(y);
			
		}
		
		if( x > z && x > y) {							// ist x gr��er z UND ist x gr��er y dann...
			
			System.out.println(z);
		}
			
		
		else {
			
			System.out.println(x);
		}
			
		
		// while Z�hlschleife bis 10
		
		int a = 0;
		
		while(a <= 10) {
			System.out.println(x);
			a++;
		}
		
		// for Schleife
		
		for(int i = 0; i < 10; i++) {
			System.out.println(z);
		}
		
		
	}

}
