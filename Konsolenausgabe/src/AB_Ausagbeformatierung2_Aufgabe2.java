
public class AB_Ausagbeformatierung2_Aufgabe2 {

	public static void main(String[] args) {

		// Festlegung der einzelnen Variablen f�r das Einsetzen in die Funktionen
		// Werte / Strings f�r die einzelnen Fakult�ten
		
		String A = "0!";
		String B = "1!";
		String C = "2!";
		String D = "3!";
		String E = "4!";
		String F = "5!";
		
		// Werte / Strings f�r die "Berechnungen" und "Ergebnisse"
		
		String G = " ";							// f�r das Leerzeichen nach dem =
		String H = "1";
		String I = "2";
		String J = "6";
		String K = "24";
		String L = "120";
	
		// Ausgabe von 0! = 1
		
		System.out.printf("%-4s =", A);					// Ausgabe von:"0!   ="
		System.out.printf("%-18s =", G);				// Ausgabe von:"                   ="
		System.out.printf("%4s%n", H);					// Ausgabe von: "   1"
														/* was macht das n in der Klammer?
														   -> gibt Zeilenumbruch an */
		
		// Ausgabe von 1! = 1
		
		System.out.printf("%-4s =", B);					// Ausgabe von: "1!   ="
		System.out.printf(" 1%-17s=", G);				// Ausgabe von: " 1                 ="
														/* was macht die "1" vor dem % Zeichen?
												   		   -> ist das was vor dem Abstand stehen soll */
		System.out.printf("%4s%n", H);					// Ausgabe von: "   1"			
														
		// Ausgabe von 2! = 2
		
		System.out.printf("%-4s =", C);					// Ausgabe von: "2!   ="
		System.out.printf(" 1 * 2%-12s =", G);			// Ausgabe von:	" 1 * 2             ="
		System.out.printf("%4s%n", I);					// Ausgabe von:	"   2" 
		
		// Ausgabe von 3! = 6
		
		System.out.printf("%-4s =", D);					// Ausgabe von: "3!   ="
		System.out.printf(" 1 * 2 * 3%-8s =",  G);		// Ausgabe von: " 1 * 2 * 3         =" 
		System.out.printf("%4s%n", J);					// Ausgabe von:	"   6" 
		
		// Ausgabe von 4! = 24
		
		System.out.printf("%-4s =", E);					// Ausgabe von: "4!   ="
		System.out.printf(" 1 * 2 * 3 * 4%-4s =", G);	// Ausgabe von: " 1 * 2 * 3 * 4     ="
		System.out.printf("%4s%n", K);					// Ausgabe von: "  24"
		
		// Ausgabe von 5! = 120
		
		System.out.printf("%-4s =", F);					// Ausgabe von: "5!   ="
		System.out.printf(" 1 * 2 * 3 * 4 * 5%s=", G);	// Ausgabe von: " 1 * 2 * 3 * 4 * 5 ="
		System.out.printf("%4s%n", L);					// Ausgabe von: " 120"

	}

}
