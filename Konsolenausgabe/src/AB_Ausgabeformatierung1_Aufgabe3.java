
public class AB_Ausgabeformatierung1_Aufgabe3 {

	public static void main(String[] args) {
		// Festlegung der Variablen
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;

		//Ausgabe der Kommazahlen mit gewŁnschter Anzahl an Kommastellen
		
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
	}

}
