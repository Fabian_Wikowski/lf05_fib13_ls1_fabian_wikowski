
public class AB_Ausgabeformatierung1_Aufgabe1 {

	public static void main(String[] args) {
		// Aufgabe 1
		
		// "Erster Teil Aufgabe 1" drucken
			System.out.println("Erster Teil Aufgabe 1: ");
						
		// "Absatz"
			System.out.println(" ");
						
		// "Ich lerne Java" ausgeben
			System.out.println("Ich lerne Java. ");
						
		// "Du lernst Java" ausgeben
			System.out.println("Du lernst Java. ");
						
		// "Wir alle lernen Java" ausgeben
			System.out.print("Wir alle lernen Java.");

		// "Absatz"
			System.out.println(" ");
			System.out.println(" ");
						
						
						
		// Zweiter Teil der Aufgabe 1
						
		// "Zweiter Teil Aufgabe 1" drucken
			System.out.println("Zweiter Teil Aufgabe 1: ");
						
		// "Absatz"
			System.out.println(" ");
						
		// "Ich lerne Java" ausgeben mit Zeilenaufschub
			System.out.print("Ich lerne Java.\n");
								
		// Fabi sagte: "Ich lerne Java!" ausgeben
			System.out.println("Fabi sagte: \"Ich lerne Java!\"");
						
		// Absatz
			System.out.println(" ");
			
		// Dritter Teil der Aufgabe 1
		
		// "Dritter Teil Aufgabe 1" drucken
			System.out.println("Dritter Teil Aufgabe 1: ");
			
		// "Absatz"
			System.out.println(" ");
			
		// Ich hei�e Fabian und bin 25 Jahre alt
			int alter = 25;
			String name = "Fabian";
			System.out.print("Ich hei�e " + name + " und bin " + alter + " Jahre alt");
			
			
		// Vierter Teil der Aufgabe 1
			
		/*Der Unterschied zwischen dem command System.out.print() und System.out.println()
		besteht darin, dass bei dem command println() einen Zeilenaufschub durchf�hrt.*/
								

	}

}
