
public class AB_Ausgabeformatierung2_Aufgabe1 {

	public static void main(String[] args) {
		
		// Ausgabe mit Leerzeichen
		System.out.println("einmal mit Leerzeichen");
		System.out.println(" ");
		
		System.out.print("    **    \n");
		System.out.print("*        *\n");
		System.out.print("*        *\n");
		System.out.print("    **    \n");

		// zus�tzlicher Absatz zwischen den Varianten f�r �bersicht
		
		System.out.println("  ");
		System.out.println("  ");
		
		// Ausgabe ohne Leerzeichen
		System.out.println("einmal ohne Leerzeichen");
		System.out.println(" ");
		
		//Festlegung der einzelnen Strings die in die Funktionen eingesetzt werden f�r die Ausgabe
		
		String A	= "**";
		String B1	= "*";
		String B2	= "*";
		String C1	= "*";
		String C2	= "*";
		String D	= "**";
		
		// Ausgabe der einzelnen Zeilen
		
		System.out.printf("%6s%n", A);		// Ausgabe: "    **"	-> Erste Zeile
		System.out.printf("%s", B1);		// Ausgabe: "*" 		-> Erster Stern in zweiter Zeile
		System.out.printf("%9s%n", B2);		// Ausgabe: "        *"	-> Zweiter Stern in zweiter Zeile
		System.out.printf("%s", C1);		// Ausgabe: "*"			-> Erster Stern in dritter Zeile
		System.out.printf("%9s%n", C2);		// Ausgabe: "        *"	-> Zweiter Stern in dritter Zeile
		System.out.printf("%6s", D);		// Ausgabe: "    **"	-> Vierte Zeile
	}

}
