
public class AB_Ausgabeformatierung2_Aufgabe3 {

	public static void main(String[] args) {
		
		// Festlegung der einzelnen Variablen f�r das Einsetzen in die entsprechenden Funktionen
		// String ... = Werte f�r Fahrenheit
		// double ... = Kommazahlen f�r Celsius
		
		String F = "Fahrenheit";
		String C = "Celsius";
		
		String A1 = "-20";
		double A2 = -28.8889;
		
		String B1 = "-10";
		double B2 = -23.3333;
		
		String C1 = "0";
		double C2 = -17.7778;
		
		String D1 = "20";
		double D2 = -6.6667;
		
		String E1 = "30";
		double E2 = -1.1111;
		
		// Ausgabe der einzelnen Tabellenzeilen / -spalten
		
		// Ausgabe Zeile 1
		System.out.printf("%-12s|", F);						// -> Ausgabe von: "Fahrenheit  |"
		System.out.printf("%10s%n", C);						// -> Ausgabe von: "   Celsius"
		System.out.println("-----------------------");		// -> Ausgabe der Linie
		
		// Ausgabe Zeile 2
		System.out.printf("%-12s|", A1);					// -> Ausgabe von: "-20         " 
		System.out.printf("%10.2f%n", A2);					// -> Ausgabe von: "    -28,89"
															/* -> das .2 gibt an dass nur 2 Kommastellen angegeben werden sollen
															   -> das f vor dem % gibt an dass es eine floatkommazahl ist */
		
		// Ausgabe Zeile 3
		System.out.printf("%-12s|", B1);					// -> Ausgabe von: "-10         |"
		System.out.printf("%10.2f%n", B2);					// -> Ausgabe von: "    -23,33"
		
		// Ausgabe Zeile 4
		System.out.printf("+%-11s|", C1);					// -> Ausgabe von: "+0          |"
															/* das + vor dem % ist das was angehangen wird f�r
															   die positive Gradzahl */
		
		System.out.printf("%10.2f%n", C2);					// -> Ausgabe von: "    -17,78"
		
		// Ausgabe Zeile 5
		System.out.printf("+%-11s|", D1);					// -> Ausgabe von: "+20         |"
		System.out.printf("%10.2f%n", D2);					// -> Ausgabe von: "     -6,67"
		
		// Ausgabe Zeile 6
		System.out.printf("+%-11s|", E1);					// -> Ausgabe von: "+30         |"
		System.out.printf("%10.2f%n", E2);					// -> Ausgabe von: "     -1,11"
		
		
	}

}
