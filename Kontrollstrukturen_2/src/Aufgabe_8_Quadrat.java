import java.util.Scanner;

public class Aufgabe_8_Quadrat {

	public static void main(String[] args) {
		
		int eingabe;
        Scanner tastatur = new Scanner(System.in);

        // Eingabe der Seitenlaenge
        System.out.print("Bitte bestimmen Sie die Seitenlaenge des Quadrats: ");
        eingabe = tastatur.nextInt();
        
        System.out.println("");

        // Ausgabe des entsprechenden Quadrates
        System.out.println("Sie haben die Seitenlänge wie folgt bestimmt: " + eingabe + "\nDas Quadrat wird nun dargestellt.\n");
        
        
        // Berechnung und Ausgabe des eigentlichen Quadrates
        for(int i = 1; i <= eingabe; i++) {
            System.out.print("* ");
        }
        
        System.out.println("");
        
        for(int i = 1; i <= eingabe - 2; i++) {
        
        	System.out.print("* ");
            
        	for(int j = 1; j <= eingabe -2 ; j++) {
            
        		System.out.print("  ");
            }
            
        	System.out.println("* ");
        }
        
        for(int i = 1; i <= eingabe; i++) {
            
        	System.out.print("* ");
        }

	}

}