import java.util.Scanner;

public class Aufgabe_6_Million {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		char moechteberechnen;
		
		do {
			
			// Eingaben des Users
			double userEinlage = eingabeEinlage(tastatur);
			double userZinssatz = eingabeZinssatz(tastatur);
			
			// Berechnung Million
			int berechnung = berechnungMillion(userEinlage, userZinssatz);
			
			System.out.println("Glueckwunsch. Sie sind in " + berechnung + " Jahren ein Millionaer." );
			
			System.out.println(" ");
			
			System.out.print("Moechten Sie einen weiteren Wert berechnen? [ja oder nein]: ");
			moechteberechnen = tastatur.next().charAt(0);
			
			System.out.println(" ");
			
		}while(moechteberechnen == 'j');
		
		if(moechteberechnen == 'n') {
			System.out.print("Bis zum naechsten Mal!");
		}	
		

	}

	
	// Methode Eingabe Einlage des Users
	public static double eingabeEinlage(Scanner tastatur) {
		
		System.out.print("Bitte geben Sie den Betrag ihrer Einlage ein: ");
		
		double einlage = tastatur.nextDouble();
		
		return einlage;
		
	}
	
	// Methode Eingabe Zinssatz des Users
	public static double eingabeZinssatz(Scanner tastatur) {
		
		System.out.print("Geben Sie den Zinssatz an mit dem ihre Einlagen verzinst sind [in Prozent]: ");
		
		double zinssatz = tastatur.nextDouble();
		
		double mathZinssatz = zinssatz / 100;
		
		return mathZinssatz;
	}
	
	// Methode Berechnung Jahre bis zum Millionär
	public static int berechnungMillion(double einlage, double zinssatz) {
		
		double summe = einlage;
		int jahreszahl = 0;
		
		while(summe < 1000000) {
            summe += summe * zinssatz;
            jahreszahl++;
        }
		
		return jahreszahl;
	}
	
	
}