import java.util.Scanner;

public class Aufgabe_5_OhmschesGesetz {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		char moechtenberechnen;
		
		
		do {
			
			String userEinheit = auswahlEinheit(tastatur);
			
			System.out.println(" ");
			
			String userEinheitGROSS = userEinheit.toUpperCase();
			
			char buchstabeEinheit = userEinheitGROSS.charAt(0);
			
			
			
			// Berechnungen der entsprechenden Einheiten
			
			switch(buchstabeEinheit) {
			
			case 'R':
				
				// Eingaben der Werte
				double userZahlSpannung1 = eingabeZahlSpannung(tastatur);
				double userZahlStrom1 = eingabeZahlStrom(tastatur);
				
				System.out.println(" ");
				
				// Berechnung
				double r = userZahlSpannung1 / userZahlStrom1;
				System.out.printf("Der Widerstand betraegt: " + "%.2f OHM", r);
				
				System.out.println(" ");
				System.out.println(" ");
				break;
				
				
			case 'U':
				
				// Eingaben der Werte
				double userZahlWiderstand1 = eingabeZahlWiderstand(tastatur);
				double userZahlStrom2 = eingabeZahlStrom(tastatur);
				
				System.out.println(" ");
				
				// Berechnung
				double u = userZahlWiderstand1 * userZahlStrom2;
				System.out.printf("Der Spannung beträgt: " + "%.2f VOLT",u);
				
				System.out.println(" ");
				System.out.println(" ");
				break;
				
				
			case 'I':
				
				// Eingaben der Werte
				double userZahlSpannung2 = eingabeZahlSpannung(tastatur);
				double userZahlWiderstand2 = eingabeZahlWiderstand(tastatur);
				
				System.out.println(" ");
				System.out.println(" ");
				
				// Berechnung
				double i =  userZahlSpannung2 / userZahlWiderstand2;
				System.out.printf("Der Strom betraegt: " +  "%.2f Ampere",i);
				
				System.out.println(" ");
				System.out.println(" ");
				break;
				
			default:
				
				System.out.println("Ihre Eingabe ist ungueltig!");
				
				System.out.println(" ");
				System.out.println(" ");
			
			}
			
			System.out.print("Moechten Sie einen weiteren Wert berechnen? [ja oder nein]: ");
			moechtenberechnen = tastatur.next().charAt(0);
			
			System.out.println(" ");
			
		} while(moechtenberechnen == 'j');
		
		if(moechtenberechnen == 'n') {
			System.out.print("Bis zum naechsten Mal!");
			
		}
		

	}


	// Methode fuer die Auswahl der auszurechnenden Einheit
	public static String auswahlEinheit(Scanner tastatur) {
		
		System.out.print("Welche Einheit hinsichtlich des Ohmschen Gesetzes moechten Sie berechnen? [Ihre Auswahlmoeglichkeiten: R fuer Widerstand | U fuer Spannung | I fuer Strom]: ");
		
		String auswahl = tastatur.next();
		
		return auswahl;
		
	}
	
	
	

	// Methode fuer die Eingabe des Wertes fuer den Widerstand
	public static double eingabeZahlWiderstand(Scanner tastatur) {
	
		System.out.print("Bitte geben Sie einen Wert fuer den Widerstand [R] ein: ");
		
		double widerstandR = tastatur.nextDouble();
		
		return widerstandR;
	
}

	
	

	// Methode fuer die Eingabe des Wertes fuer die Spannung
	public static double eingabeZahlSpannung(Scanner tastatur) {
		
		System.out.print("Bitte geben Sie einen Wert fuer die Spannung [U] ein: ");
		
		double spannungU = tastatur.nextDouble();
		
		return spannungU;
	}

	
	
	
	// Methode fuer die Eingabe des Wertes fuer den Strom
	public static double eingabeZahlStrom(Scanner tastatur) {
		
		System.out.print("Bitte geben Sie einen Wert fuer den Strom [I] ein: ");
		
		double stromI = tastatur.nextDouble();
		
		return stromI;
	}
		
		
	}