import java.util.Scanner;
public class Aufgabe_5_BMI {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        char moechtenberechnen;
        
        
        do {
            
            double groesse = koerpergroesse(tastatur);
            double gewicht = koerpergewicht(tastatur);
            String geschlecht = abfrageGeschlecht(tastatur);
        
        
        
            char buchstabe = geschlecht.charAt(0);
            
                // wenn maennlich
                
                if(buchstabe == 'm') {
                    
                    double bmi = (gewicht / (groesse * groesse));
                    System.out.printf("Ihr BMI lautet " + "%.2f \n", bmi);
                    
                    if(bmi < 20) {
                        
                        System.out.print("Damit gelten Sie als untergewichtig. \n");
                        System.out.println(" ");
                        
                    }
                    
                    else if(bmi == 20 && bmi <= 25) {
                        
                        System.out.print("Damit gelten Sie als normalgewichtig. \n");
                        System.out.println(" ");
                        
                    }
                    
                    else {
                        System.out.print("Damit gelten Sie als uebergewichtig.\n");
                        System.out.println(" ");
                    }
                    
                }
                
                // wenn weiblich
                
                if(buchstabe == 'w') {
                    
                    double bmi = (gewicht / (groesse * groesse));
                    System.out.printf("Ihr BMI lautet " + "%.2f \n", bmi);
                    
                    if(bmi < 19) {
                        
                        System.out.print("Damit gelten Sie als untergewichtig. \n");
                        System.out.println(" ");
                        
                    }
                    
                    else if(bmi == 19 && bmi <= 24) {
                        
                        System.out.print("Damit gelten Sie als normalgewichtig. \n");
                        System.out.println(" ");
                        
                    }
                    
                    else {
                        System.out.print("Damit gelten Sie als uebergewichtig. \n");
                        System.out.println(" ");
                    }
                    
                }
                
                // sonstige
                
                if(buchstabe != 'm' && buchstabe != 'w') {
                    System.out.print("Ihre Eingabe ist nicht korrekt! \n ");
                    System.out.println(" ");
                }
                
            System.out.println("Möchten Sie einen weiteren BMI Wert berechnen? [ja oder nein] ");
            moechtenberechnen = tastatur.next().charAt(0);
            
            System.out.println(" ");
            
} while(moechtenberechnen == 'j');
        
}
        

// Methode Eingabe der Koerpergroesse
public static double koerpergroesse(Scanner tastatur) {
    
    System.out.print("Bitte geben Sie Ihre Koerpergroesse in Centimetern ein: ");
    System.out.println(" ");
    
    double groesse = tastatur.nextDouble();
    
    double groesseM = groesse / 100;
    
    return groesseM;
     
}

// Methode Eingabe Koerpergewicht
public static double koerpergewicht(Scanner tastatur) {
    
    System.out.print("Bitte geben Sie ihr Gewicht in Kilogramm an [maximal 2 Kommastelle]): ");
    System.out.println(" ");
    
    double gewicht = tastatur.nextDouble();
    
    return gewicht;
    
    
}

// Methode Eingabe Geschlecht
public static String abfrageGeschlecht(Scanner tastatur) {
    
    System.out.print("Bitte geben Sie ihr Geschlecht an: ");
    System.out.println(" ");
    
    String geschlecht = tastatur.next();
    
    String geschlechtklein = geschlecht.toLowerCase();
    
    return geschlechtklein;
    
    
    }

    }
