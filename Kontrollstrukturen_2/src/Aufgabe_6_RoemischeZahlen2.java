import java.util.Scanner;

public class Aufgabe_6_RoemischeZahlen2 {

	public static void main(String[] args) {
		int ergebnis = 0;
		Scanner tastatur = new Scanner(System.in);

		// Eingabe von roemischen Zahlen
		System.out.println("Bitte geben Sie eine roemische Zahl ein. Maximal 3 gleiche Zeichen hintereinander.");
		String eingabe = tastatur.next();
		eingabe = eingabe.toUpperCase();

		// List der Chars erstellen
		char[] charList = eingabe.toCharArray();

		// Test auf unerlaubte Zahlenfolgen, wie z.B. XXXX = false, waehrend XXX = true,
		// VV = false, ...
		if (inputTest(charList) == false) {
			System.out.println("Eingabe Fehler, bitte an die Regeln der roehmischen Zahlen halten!");
			System.exit(0);
		}

		// Dezimalliste mit der Laenge der charList intialisieren
		int[] dezimalList = new int[charList.length];

		// Liste aus Dezimal Zahlen erstellen
		for (int i = 0; i < charList.length; i++) {
			if (alsDezimal(charList[i]) == -1) {
				System.out.println("Fehler! Bitte nur 'M', 'D', 'C', 'L', 'X', 'V', 'I' benutzen.");
				System.exit(0);
			} else {
				dezimalList[i] = alsDezimal(charList[i]);
			}
			// System.out.println("Array dezimalList an Stelle " + i + " ist die Zahl: " + dezimalList[i]);
		}

		// Subtraktosnregelcheck
		if(subtraktionsRegelCheck(dezimalList) == false) {
			System.out.println("Fehler! Subtraktionsregel nicht beachtet! Bsp.: IIV ungueltig, IV g�ltig. ");
			System.exit(0);
		}
		
		// zahlenKorrektur() geht das Array mehrhmals durch um alle Zahlen zu finden die
		// Korregiert werden muessen
		// wenn i < i+1 ist, wird dann durch das Ergebnis aus i+1 - i die Stelle i
		// ersetzt und Stelle
		// i+1 bekommt eine 0
		// am ende erhaelt man ein Korrektes Array mit allen Zahlen in Reihenfolge
		for (int i = 0; i < dezimalList.length * 2; i++) {
			dezimalList = zahlenKorrektur(dezimalList);
		}

		// Ergebnis berechnen 
		for (int i = 0; i < dezimalList.length; i++) {
			ergebnis += dezimalList[i];
		}

		// Ausgabe & Ende des Programms
		System.out.println("Eingabe: " + eingabe + "\nErgebnis: " + ergebnis);
	}

	
	// Input Ueberpruefung
	public static boolean inputTest(char[] charList) {
		for (int i = 0; i < charList.length; i++) {
			// Dreifache roemische Zahlen erkennen
			if (i + 3 < charList.length) {
				if (charList[i] == charList[i + 1] && charList[i + 1] == charList[i + 2]
						&& charList[i + 2] == charList[i + 3]) {
					return false;
				}
			}

			// Doppelte roemische Zahlen die ungueltig sind erkennen
			// ehemalig wurden z.B. VV durch X ersetzt, aber das will die Aufgabe nicht
			if (i + 1 < charList.length) {
				if (charList[i] == charList[i + 1] && charList[i] == 'V') {
					return false;
				} else if (charList[i] == charList[i + 1] && charList[i] == 'L') {
					return false;
				} else if (charList[i] == charList[i + 1] && charList[i] == 'D') {
					return false;
				}
			}
		}
		return true;
	}

	// Funktion um Roemische Zahlen als Dezimalzahlen zu definieren
	public static int alsDezimal(char a) {
		if (a == 'I') {
			return 1;
		}

		if (a == 'V') {
			return 5;
		}

		if (a == 'X') {
			return 10;
		}

		if (a == 'L') {
			return 50;
		}

		if (a == 'C') {
			return 100;
		}

		if (a == 'D') {
			return 500;
		}

		if (a == 'M') {
			return 1000;
		}

		return -1;
	}
	
	public static boolean subtraktionsRegelCheck(int[] dezimalList) {
		for(int i = 0; i< dezimalList.length; i++) {
			if(i+1 < dezimalList.length) {
				if(dezimalList[i] <= dezimalList[i+1]) {
					if(i+2 < dezimalList.length) {
						if(dezimalList[i+1] < dezimalList[i+2] && dezimalList[i+1] != 0) {
							return false;
						}
					}
				}
			}
			
		}
		return true;
	}

	// Methode zum korrekten ersetzen der einzelnen Zahlen im Array
	public static int[] zahlenKorrektur(int[] dezimalList) {
		for (int i = 0; i < dezimalList.length; i++) {
			if (i + 1 < dezimalList.length && dezimalList[i] < dezimalList[i + 1]) {
				dezimalList[i] = dezimalList[i + 1] - dezimalList[i];
				dezimalList[i + 1] = 0;
			}
		}
		return dezimalList;
	}
}