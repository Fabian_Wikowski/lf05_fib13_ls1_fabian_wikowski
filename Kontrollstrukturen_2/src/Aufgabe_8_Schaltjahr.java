import java.util.Scanner;

public class Aufgabe_8_Schaltjahr {

	
	// Main Methode
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		char moechteSchaltjahr;
		
		do {
			
			int userJahr = eingabeJahr(tastatur);
			
			pruefungVor1582(userJahr);
			
			pruefungNach1582(userJahr);	
			
			System.out.println("Möchten Sie bei einem weiterem Jahr wissen ob es ein Schaltjahr ist? [ja oder nein] ");
			moechteSchaltjahr = tastatur.next().charAt(0);
			
			System.out.println(" ");
			
			
			
		} while(moechteSchaltjahr == 'j');
		
		if(moechteSchaltjahr == 'n') {
			System.out.print("Bis zum naechsten Mal!");
			
		}

	}

	
	
	// Eingabe des Jahrs vom User
	public static int eingabeJahr(Scanner tastatur) {
		
		System.out.print("Bitte geben Sie ihr Jahr ein, welches Sie prüfen möchten: ");
		
		System.out.println(" ");
		
		int userJahr = tastatur.nextInt();
		
		return userJahr;
	}
	
	
	// Pruefung Schaltjahr vor 1582
	public static int pruefungVor1582(int userJahr) {
		
			if(userJahr <= 1582) {
			
				if(userJahr % 4 == 0) {
				
					System.out.print("Ihre eingegebene Jahreszahl ist ein Schaltjahr!");
				
				}
			
			else
				System.out.print("Ihre eingegebene Jahreszahl ist KEIN Schaltjahr!.");
			
		}
			System.out.println(" ");
			System.out.println(" ");
			
			return userJahr;
		
	}
	
	
	// Pruefung Schaltjahr nach 1582
	public static int pruefungNach1582(int userJahr) {
		
		if(userJahr > 1582) {
			
			if(userJahr % 4 == 0) {
				if(userJahr % 100 == 0) {
					if(userJahr % 400 == 0) {
						System.out.print("Ihre eingegebene Jahreszahl ist ein Schaltjahr!");
					}
					else
						System.out.print("Ihre eingegebene Jahreszahl ist KEIN Schaltjahr!");
				}
				else
					System.out.print("Ihre eingegebene Jahreszahl ist KEIN Schaltjahr!");
			
			}
			else
				System.out.print("Ihre eingegebene Jahreszahl ist KEIN Schaltjahr!");
			
			}
		
		System.out.println(" ");
		System.out.println(" ");
		
		return userJahr;
	}
	
}