import java.util.Scanner;

public class Aufgabe_4_Taschenrechner {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie die erste Zahl ein: ");
		double zahl1 = tastatur.nextDouble(); 
		
		System.out.print("Bitte geben Sie die zweite Zahl ein: ");
		double zahl2 = tastatur.nextDouble();
		
		
		// Abfrage welche Operation
		System.out.print("Welche Rechenoperation moechten Sie mit den von Ihnen eingegebenen Zahlen durchfuehren? [ Ihre Auswahl: + | - | * | / ]: ");
		
		String eingabeOperation = tastatur.next();
		
		char zeichen = eingabeOperation.charAt(0);
		
		
		
		// Berechnungen mit Ausgabe
		switch(zeichen) {
		
			case '+':
			
				double ergebnisAdd = zahl1 + zahl2;
				System.out.print("Ihr Ergebnis der Addition lautet: " + ergebnisAdd);
				break;
			
			case '-':
			
				double ergebnisSub = zahl1 - zahl2;
				System.out.print("Ihr Ergebnis der Subtraktion lautet: " + ergebnisSub); 
				break;
			
			case '*':
			
				double ergebnisMulti = zahl1 * zahl2;
				System.out.print("Ihr Ergebnis der Multiplikation lautet: " + ergebnisMulti );
				break;
			
			case '/':
			
				double ergebnisDivi = zahl1 / zahl2;
				System.out.print("Ihr Ergebnis der Division lautet: " + ergebnisDivi);
				break;
			
			default:
				System.out.println("Ihre Eingabe ist ungueltig!");
			
    }
    
}
}
