import java.util.Scanner;

public class Aufgabe_8_Matrix {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		char moechteberechnen;
		
		do {
			// Eingabe zwischen 2 und 9
			int zahlEingabe = userEingabe(tastatur);
			
			// Pruefung der Eingabe und Ausgabe der entsprechenden Matrix
			int pruefung = pruefungVielfaches(zahlEingabe);	
			
			
			
			System.out.println(" ");
			
			System.out.print("Moechten Sie einen weiteren Wert berechnen? [ja oder nein]: ");
			moechteberechnen = tastatur.next().charAt(0);
			
			System.out.println(" ");
			
		}while(moechteberechnen == 'j');
		
		if(moechteberechnen == 'n') {
			System.out.print("Bis zum naechsten Mal!");
		}
		
		
		
		
		}
		
	
	// Methode fuer die EIngabe des Users
	public static int userEingabe(Scanner tastatur) {
		
		System.out.println("Dies ist ein Programm fuer die Anzeige einer Matrix. Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int eingabe = tastatur.nextInt();
		System.out.print("0 ");
		
		return eingabe;
		
	}

	
	// Methode f�r die Pruefung der Eingabe und Ausgabe der jeweiligen Matrix
	public static int pruefungVielfaches(int wertUser) {
		
		int i = 1;
		int j = 1;
		int zahl1;
		int zahl2;
		int zahl3;
		
		if (wertUser >= 2 && wertUser <= 9) {
			//Test von Vielfache von eingabe
			while(i < 100 && j < 100) {
				zahl1 = i / 10;
				zahl2 = i % 10;
				zahl3 = zahl1 + zahl2;
				if(j % 10 == 0) {
					System.out.println(" ");
				}
			
				if(i % wertUser == 0) {
					System.out.print("* ");
				}
				else if(zahl3  == wertUser) {
					System.out.print("* ");
				}
				
				else if (i / 10 == wertUser) {
					System.out.print("* ");
				}
				
				else if (i % 10 == wertUser) {
					System.out.print("* ");
				}
				
				else {
					System.out.print(i + " ");
				}
				i = i + 1;
				j = j + 1;
			}
		}
		
		return wertUser;
	}
}
