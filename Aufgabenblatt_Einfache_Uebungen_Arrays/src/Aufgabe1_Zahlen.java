// Aufgabe 1
// Schreiben Sie ein Programm „Zahlen“, in welchem ein ganzzahliges Array der Laenge 10 deklariert wird.
// Anschliessend wird das Array mittels Schleife mit den Zahlen von 0 bis 9 gefüllt.
// Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer Schleife auf der Konsole aus.



public class Aufgabe1_Zahlen {

public static void main(String[] args) {
    
    int[] zahlenArray = new int[10];        // Array mit 10 Stellen wird erstellt

    System.out.print("Das Array mit der Länge von 10 hat folgende Werte: ");

    // Array befuellen mit Werten
    for(int i = 0; i < zahlenArray.length; i++){            // Initialisierung eines Zählers i
                                                                              // Bedingung Schleife durchlaufen werden soll solange i < Länge Array
                                                                             // nach Durchgang i um 1 erhöhen

        zahlenArray[i] = i;                                         // Stelle des Arrays mit dem Wert von i befüllen
        System.out.print(zahlenArray[i] + " ");         // Ausgabe des Wertes an entsprechender Stelle des Arrays
    }
}    
}
