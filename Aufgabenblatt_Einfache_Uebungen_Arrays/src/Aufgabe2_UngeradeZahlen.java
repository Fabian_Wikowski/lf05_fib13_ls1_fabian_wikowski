// Aufgabe 2:
// Das zu schreibende Programm „UngeradeZahlen“ ist aehnlich der Aufgabe 1.
// Sie deklarieren wiederum ein Array mit 10 Ganzzahlen.
// Danach fuellen Sie es mit den ungeraden Zahlen von 1 bis 19 und geben den Inhalt des Arrays über die Konsole aus
// (Verwenden Sie Schleifen!).


public class Aufgabe2_UngeradeZahlen {
    public static void main(String[] args) {

        int[] zahlen = new int[10];                 // Array mit 10 Stellen wird erstellt
        int x = 0;                                           // Variable zum hinterlegen der ungeraden Zahlen wird erstellt

        System.out.print("Die ungeraden Zahlen bis 20 lauten: ");

        for (int i = 0; i <= 19; i++) {             // Initialisierung eines Zählers, welcher bis 19 läuft

            if(i %2 == 0 && i <= 19) {          // Prüfung ob Zähler durch 2 teilbar und somit gerade ist und ob er kleiner gleich 19 ist
                // tue nichts
            }

            else{                                       // Anweisung falls Zähler nicht durch 2 teilbar ist und somit ungerade ist
                zahlen[x] = i;                     // Variable x wird gleich dem Zähler gesetzt
                x +=1;                               // Variable x wird um den Wert 1 erhöht
            }
        }

        System.out.print(java.util.Arrays.toString(zahlen));

    }
 }

